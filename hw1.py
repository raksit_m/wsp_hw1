# coding: utf-8
'''
Created on 17 �.�. 2559

@author: Earth
'''

import urllib.request
u = urllib.request.urlopen('http://ctabustracker.com/bustime/map/getBusesForRoute.jsp?route=22')
data = u.read()
f = open('rt22.xml', 'wb')
f.write(data)
f.close()
print('Wrote rt22.xml')

office_lat = 41.980262
office_long = -87.668452

from xml.etree.ElementTree import parse
import webbrowser
from math import sin, cos, sqrt, atan2, radians

doc = parse('rt22.xml')
R = 6373.0
web = 'https://maps.googleapis.com/maps/api/staticmap?center=' + str(office_lat) + ',' + str(office_long) + '&zoom=13&size=800x600' \
                                                                                                            '&markers=color:red%7Clabel:O%7C' + str(office_lat) + ',' + str(office_long)
for bus in doc.findall('bus'):
    lat = float(bus.findtext('lat'))
    long = float(bus.findtext('lon'))

    dlat = radians(lat) - radians(office_lat)
    dlong = radians(long) - radians(office_long)

    a = sin(dlat / 2) ** 2 + cos(radians(office_lat)) * cos(radians(lat)) * sin(dlong / 2) ** 2
    c = 2 * atan2(sqrt(a), sqrt(1 - a))

    distance = (R * c) * 0.621371192

    if distance <= 0.5:
        busid = bus.findtext('id')
        direction = bus.findtext('d')
        print(busid, direction, lat)
        web += '&markers=color:blue%7Clabel:B%7C' + str(lat) + ',' + str(long)
webbrowser.open(web)
